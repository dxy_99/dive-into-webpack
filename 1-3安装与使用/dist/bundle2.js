/******/ (function(modules) { // webpackBootstrap mark Webpack引导
/******/ 	// The module cache mark 模块缓存
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function mark 要求功能
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache mark 检查模块是否在缓存中
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache) mark 创建一个新模块（并将其放入缓存）
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function mark 执行模块功能
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded mark 将模块标记为已加载
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module mark 返回模块的导出
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__) mark 公开模块对象（__webpack_modules__）
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache mark 公开模块缓存
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports mark 为和谐出口定义getter函数
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules mark getDefaultExport函数用于与非和谐模块兼容
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

// 通过 CommonJS 规范导入 show 函数
const show = __webpack_require__(1);
// 执行 show 函数
show('Webpack');

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// 操作 DOM 元素，把 content 显示到网页上
function show(content) {
  window.document.getElementById('app').innerText = 'Hello,' + content;
}

// 通过 CommonJS 规范导出 show 函数
module.exports = show;


/***/ })
/******/ ]);
